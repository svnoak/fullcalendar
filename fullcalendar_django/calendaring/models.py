from django.db import models
from django import forms

# Create your models here.

class Color(models.Model):
    color = models.CharField(max_length=50)
    def __str__(self):
        return self.color

class startTime(forms.ModelForm):
    start_time = forms.TimeField(input_formats='%H:%M',required=True)
    def __str__(self):
        return self.start_time

class Event(models.Model):
    event_id = models.AutoField(primary_key=True)
    event_name = models.CharField(max_length=255,null=True)
    start_date = models.DateField(null=True)
    start_time = models.TimeField(null=True, blank=True)
    end_date = models.DateField(null=True)
    end_time = models.TimeField(null=True, blank=True)
    all_day = models.BooleanField()
    color = models.CharField(max_length=7, null=True)

    def __str__(self):
        return self.event_name