from django.apps import AppConfig


class CalendaringConfig(AppConfig):
    name = 'calendaring'
