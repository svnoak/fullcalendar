from django.contrib import admin
from .models import Event, Color

# Register your models here.
admin.site.register(Event)
admin.site.register(Color)